

import java.sql.*;
import java.util.Scanner;

// REMEMBER to edit the name of the database in the init_db() method
public class CA_3
{
	static Connection con = null;
	static Statement stmt = null;
	static ResultSet rs = null;
	static Scanner in = new Scanner(System.in);
	
	public static void main(String[] args) 
	{
		init_db();  // open the connection to the database
		
		int menuChoice = 0; // variable used to store main menu choice
		final int STOP_APP = 6; //value from menu that is used to quit the application
		
		while (menuChoice != STOP_APP)
		{
			displayMainMenu(); //display the primary men
			if (in.hasNextInt())
			{
				//get the menu choice from the user
				menuChoice = in.nextInt();
								
				switch (menuChoice)
				{
				case 1:
					displayAllProducts(); //The code for this method is already done for you below
					break;
				case 2:
					displayParticularProduct(); //You need to code this method below
					break;
				case 3:
					addNewProduct(); //You need to code this method below
					break;
				case 4:
					addNewPartToProduct(); //You need to code this method below
					break;
				case 5: 
					deleteProduct(); //You need to code this method below
					break;
				case 6:
					System.out.println("Program is closing...");
					cleanup_resources();  // close the connection to the database when finished program
					break;
				default:
					System.out.println("You entered an invalid choice, please try again...");	
				}
			}
			else
			{
				//clear the input buffer and start again
				in.nextLine();
				System.out.println("You entered an invalid choice, please try again...");
			}
		}		
	}
	
	public static void addNewPartToProduct() 
	{
		try {
			displayAllProducts();
			
			System.out.print("Please select the product ID you wish to add a part to: ");
			int prod = in.nextInt();
			String str = "Select count(*) from product where prod_id = "+prod+";";
			rs = stmt.executeQuery(str);
			if(rs.next()) {
				int res = rs.getInt(1); 
				if(res == 0) {
					System.out.println("This product doesn't exsist!");
				}
				else {
					System.out.print("Please enter the part ID: ");
					int partID = in.nextInt();
					
					System.out.print("Please enter the part name you wish to add: ");
					String partName = in.next();
					System.out.print("PLease enter the part description: ");
					String partDesc = in.next();
					System.out.print("PLease enter the cost of the product: ");
					int partCost = in.nextInt();
					
					String insertPart = "INSERT INTO part VALUES (?,?,?,?,?)";
					PreparedStatement pstmt = con.prepareStatement(insertPart);
					pstmt.setInt(1, prod);
					pstmt.setInt(2, partID);
					pstmt.setString(3, partName);
					pstmt.setString(4, partDesc);
					pstmt.setInt(5, partCost);

					pstmt.executeUpdate();
				}
			}
			
			System.out.println();
			System.out.println("Here is your updated table:");
			displayAllProducts();
			System.out.println();

			
		}
		catch(SQLException sqle) {
			System.out.println();
			System.out.println("Failed to create new part to product!");
			System.out.println(sqle.getMessage());
			System.out.println();
		}
	}
	
	public static void addNewProduct() 
	{
		
		try
		{
			
			System.out.println();
			System.out.print("Enter the product name: ");
			String prod_name = in.next();
			System.out.print("Enter the products description: ");
			String prod_description = in.next();
		
			String str = "INSERT INTO product VALUES (null,?,?)";
			PreparedStatement pstmt = con.prepareStatement(str);
			pstmt.setString(1, prod_name);
			pstmt.setString(2, prod_description);

			pstmt.executeUpdate();

			System.out.println();
			System.out.println("Here is your updated table:");
			displayAllProducts();
			System.out.println();

		}
		catch (SQLException sqle) 
		{
			System.out.println();
			System.out.println("Error: failed to create new product");
            System.out.println(sqle.getMessage());
            System.out.println();
		}
		
	}
	
	public static void displayParticularProduct() 
	{
		try {
			displayAllProducts();
			
			System.out.print("Please enter the product ID you wish to view: ");
			String prod = in.next();

			String str = "Select count(*) from product where prod_id = "+prod+";";
			rs = stmt.executeQuery(str);
			
			if(rs.next()) {
				int res = rs.getInt(1); 
				if(res == 0) {
					System.out.println("This product doesn't exsist!");
				}
				else {
					String str2 = "Select part_id, part.name, part.description, cost "+
					"from product, part where product.prod_id = part.prod_id and product.prod_id = "+prod+";";
					rs = stmt.executeQuery(str2);
							
					System.out.println("Here is the part details for product ID: "+prod);
		            System.out.println();
		            while (rs.next())
		            {
		                int partID = rs.getInt("part_id");
		                String name = rs.getString("name");
		                String desc = rs.getString(3);
		                double cost = rs.getDouble(4);
		                
		                System.out.printf("%-4d%20s%30s%8.2f\n", partID, name, desc, cost);
		            } 
		            System.out.println();
				}
			}
		}
		catch(SQLException sqle) {
			System.out.println();
			System.out.println("Failed to display product!");
			System.out.println(sqle.getMessage());
			System.out.println();
		}
	}
	
	public static void deleteProduct() 
	{
		try {
			System.out.print("Please enter the product ID you wish to view: ");
			String prod = in.next();

			String str = "Select count(*) from product where prod_id = "+prod+";";
			rs = stmt.executeQuery(str);
			
			if(rs.next()) {
				int res = rs.getInt(1); 
				if(res == 0) {
					System.out.println("This product doesn't exsist!");
				}
				else {
					String str2 = "DELETE FROM product where prod_id = "+prod+";"; 
					rs = stmt.executeQuery(str2);
					
					System.out.println("You have deleted product number "+prod);
		            System.out.println();
			
				}
			}
		}
		catch(SQLException sqle) {
			System.out.println();
			System.out.println("Failed to delete product!");
			System.out.println(sqle.getMessage());
			System.out.println();
		}
		//1: You need to get the user to select an existing product first, display all the existing products and
		//   let the user select a prod_id that they wish to delete:
		//2: Ensure that the prod_id they selected exists (query the database for a count of products with that prod_id
		//   if the count comes back as 0, then it doesn't exist if its a 1 then it does.
		//3: Delete any part that has the associated prod_id
		//4: Delete the product with the selected prod_id
		
		displayAllProducts(); 
	}
	
	public static void displayAllProducts() 
	{
		
		//1: Query the database for all products and also get the total cost (sum) and total number of parts(count)
		//2: Display the result set in an appropriate manner
		String str = "Select pr.prod_id, pr.name, pr.description, sum(pa.cost) as 'Total Cost',count(pa.prod_id) as 'No. of Parts' from product as pr, part as pa" + 
			    " where pr.prod_id = pa.prod_id group by pr.prod_id union Select pr.prod_id, pr.name, pr.description, 0 as cost, 0 as parts from product as pr where not exists" + 
			    " (select * from part as pa, product as pr2 where pa.prod_id = pr2.prod_id  and pr.prod_id = pa.prod_id);";  
			          
        try
        { 
            rs = stmt.executeQuery(str); 
            System.out.println();
            while (rs.next()) 
            { 
                int pr_id = rs.getInt("prod_id"); 
                String name = rs.getString("name"); 
                String desc = rs.getString(3); 
                double cost = rs.getDouble(4); 
                int parts = rs.getInt(5); 
                System.out.printf("%-4d%20s%30s%8.2f%5d\n", pr_id, name, desc, cost, parts); 
            } 
            System.out.println();
              
        } 
        catch (SQLException sqle) 
        { 
        	System.out.println();
            System.out.println("Error: failed to display all products."); 
            System.out.println(sqle.getMessage());
            System.out.println(str);
            System.out.println();
        } 
	}
	
	public static void displayMainMenu()
	{
		System.out.println("Main Menu\n");
		System.out.println("1: Display all products");
		System.out.println("2: Display a particular product ");
		System.out.println("3: Create a new product ");
		System.out.println("4: Add new parts to existing product ");
		System.out.println("5: Delete an existing product ");
		System.out.println("6: Exit application\n");
		System.out.print("Enter your choice: ");		
	}
	
	public static void cleanup_resources()
	{
		try
		{
			con.close();
		}
		catch (SQLException sqle)
		{
			System.out.println("Error: failed to close the database");
		}
	}
	
	public static void init_db()
	{
		try
		{
			Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
			String url="jdbc:mysql://localhost:3306/assignment?useTimezone=true&serverTimezone=UTC";
			con = DriverManager.getConnection(url, "root", "kildare1");
			stmt = con.createStatement();
		}
		catch(Exception e)
		{
			System.out.println();
			System.out.println("Error: Failed to connect to database\n"+e.getMessage());
			System.out.println();
		}
	}
}